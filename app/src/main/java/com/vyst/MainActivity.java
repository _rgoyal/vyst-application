package com.vyst;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.vyst.fragments.CartFragment;
import com.vyst.fragments.HomeFragment;
import com.vyst.fragments.OrderFragment;
import com.vyst.fragments.ProfileFragment;
import com.vyst.fragments.ReminderFragment;
import com.vyst.utils.BaseActivity;
import com.vyst.view.BottomNavigationViewHelper;

public class MainActivity extends BaseActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    replace(new HomeFragment());
                    return true;
                case R.id.navigation_category:
                    replace(new OrderFragment());
                    return true;
                case R.id.navigation_profile:
                    replace(new ProfileFragment());
                    return true;
                case R.id.navigation_notification:
                    replace(new ReminderFragment());
                    return true;
                case R.id.navigation_cart:
                    replace(new CartFragment());
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        replace(new HomeFragment());
    }

    @Override
    protected void setContextView() {
        setContentView(R.layout.activity_main);
    }

    public void replace(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment).commit();
    }
}
