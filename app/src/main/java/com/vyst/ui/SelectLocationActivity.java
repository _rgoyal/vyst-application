package com.vyst.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vyst.R;
import com.vyst.model.LocationList;
import com.vyst.model.LocationModel;
import com.vyst.network.NetworkAdaper;
import com.vyst.utils.BaseActivity;
import com.vyst.utils.ProgressDialogUtil;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectLocationActivity extends Activity implements View.OnClickListener {


    private ListView locationList;
    Adapter adapter;
    private ImageButton crossBtn;
    private TextView noDataTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_select_location);
        initView();


        callNetworkForLocations();

    }

    private void initView() {
        locationList = (ListView) findViewById(R.id.location_list);
        crossBtn = (ImageButton) findViewById(R.id.cross_btn);
        crossBtn.setOnClickListener(this);

        noDataTxt = (TextView) findViewById(R.id.no_data_txt);
    }


    private void callNetworkForLocations() {

        ProgressDialogUtil.showProgressDialog(SelectLocationActivity.this);


        NetworkAdaper.getInstance().getNetworkServices().getLocationList().enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Call<LocationModel> call, Response<LocationModel> response) {
                ProgressDialogUtil.hideProgressDialog();
                try {
                    if (response.body().getStatus() == 200) {
                        setAdapter(response.body().getData());

                    } else {
                        showSnackBar(response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LocationModel> call, Throwable t) {
                ProgressDialogUtil.hideProgressDialog();
                showToast(t.getMessage());
            }
        });
    }

    private void setAdapter(List<LocationList> location) {

        if (location != null && !location.isEmpty()) {
            adapter = new Adapter(SelectLocationActivity.this, location);
            locationList.setAdapter(adapter);
            locationList.setVisibility(View.VISIBLE);
            noDataTxt.setVisibility(View.GONE);
        }else {
            noDataTxt.setVisibility(View.VISIBLE);
            locationList.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cross_btn:
                onBackPressed();
                break;
        }
    }

    class Adapter extends BaseAdapter {
        public List<LocationList> locationArrList;
        Activity context;
        LayoutInflater l;
        ViewHolder holder;

        public Adapter(Activity context, List<LocationList> locationArrList) {
            this.locationArrList = locationArrList;
            this.context = context;
            l = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(List<LocationList> locationArrList) {
            this.locationArrList = locationArrList;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return locationArrList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = l.inflate(R.layout.adapter_select_location_child_layout, null);
                holder = new ViewHolder();
                holder.location = (TextView) convertView.findViewById(R.id.location);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            final LocationList model = locationArrList.get(position);

            try {
                String phone = (model.getLocationAddress() != null && !model.getLocationAddress().isEmpty() ? model.getLocationAddress() : "");
                holder.location.setText(phone);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", (Serializable) model);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", (Serializable) model);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });

            return convertView;
        }


        class ViewHolder {
            public TextView location;

        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
