package com.vyst.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vyst.MainActivity;
import com.vyst.R;
import com.vyst.utils.BaseActivity;
import com.vyst.utils.PrefHelper;

public class SplashActivity extends BaseActivity {


    private int SPLASH_TIME_OUT = 700;

    private TextView startBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    @Override
    protected void setContextView() {
        setContentView(R.layout.activity_splash);
    }

    private void init() {

        startBtn = (TextView) findViewById(R.id.start_btn);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!PrefHelper.getInstance().getUserId().isEmpty()) {
                    moveToMain();
                } else {
                    moveToLogin();
                }
            }
        });

    }

    private void moveToLogin() {
        Intent intent_home = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent_home);
        finish();
    }

    private void moveToMain() {
        Intent intent_home = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent_home);
        finish();
    }


}
