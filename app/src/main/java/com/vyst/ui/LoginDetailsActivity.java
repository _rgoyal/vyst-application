package com.vyst.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.vyst.MainActivity;
import com.vyst.R;
import com.vyst.model.LocationList;
import com.vyst.model.RegisterModel;
import com.vyst.model.RegisterPostData;
import com.vyst.network.NetworkAdaper;
import com.vyst.utils.AppConstant;
import com.vyst.utils.BaseActivity;
import com.vyst.utils.ProgressDialogUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginDetailsActivity extends BaseActivity implements View.OnClickListener {



    private TextInputLayout inputLayoutName;
    private TextInputLayout inputLayoutLocation;
    private TextInputLayout inputLayoutAddress;
    private String phone, locationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            phone = getIntent().getStringExtra(AppConstant.PHONE);
        } catch (Exception e) {
            phone = "";
            e.printStackTrace();
        }
        initView();
    }

    private void initView() {

        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutLocation = (TextInputLayout) findViewById(R.id.input_layout_location);
        inputLayoutAddress = (TextInputLayout) findViewById(R.id.input_layout_address);
        findViewById(R.id.order_btn).setOnClickListener(this);
        findViewById(R.id.back_btn).setOnClickListener(this);
        getLocationEdt().setOnClickListener(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstant.LOCATION_REQUEST_CODE) {

            try {
                locationID = "";
                if (resultCode == Activity.RESULT_OK) {

                    if (data != null) {
                        try {

                            LocationList model = (LocationList) data
                                    .getSerializableExtra("result");
                            if(model!=null ){

                                getLocationEdt().setText(model.getLocationAddress().trim());
                                locationID= model.getLocationId().toString();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }else {
//                    showSnackBar("Filter Cancelled!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.location_edt:
                //TODO implement
                Intent intent=new Intent(LoginDetailsActivity.this, SelectLocationActivity.class);
                startActivityForResult(intent,AppConstant.LOCATION_REQUEST_CODE);
                break;
            case R.id.back_btn:
                //TODO implement
                onBackPressed();
                break;
            case R.id.order_btn:
                //TODO implement

                if (validateData()) {
                    callNetworkForRegister();
                }

                break;
        }
    }

    private boolean validateData() {
        if (getNameEdt().getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.str_please_enter_name));
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }
        if (getLocationEdt().getText().toString().trim().isEmpty()) {
            inputLayoutLocation.setError(getString(R.string.str_please_select_location));
            return false;
        } else {
            inputLayoutLocation.setErrorEnabled(false);
        }

        if (getAddressEdt().getText().toString().trim().isEmpty()) {
            inputLayoutAddress.setError(getString(R.string.str_please_enter_address));
            return false;
        } else {
            inputLayoutAddress.setErrorEnabled(false);
        }

        return true;
    }

    private void callNetworkForRegister() {

        ProgressDialogUtil.showProgressDialog(context);

        RegisterPostData data = new RegisterPostData();
        data.setName(getNameEdt().getText().toString().trim());
        data.setLocationAddress(getAddressEdt().getText().toString().trim());
        data.setLocationId(locationID);
        data.setPhone(phone);

        NetworkAdaper.getInstance().getNetworkServices().register("application/json", data).enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                ProgressDialogUtil.hideProgressDialog();
                try {
                    if (response.body().getStatus() == 200) {
                        showSnackBar(response.body().getMessage());
                        Intent intent= new Intent(LoginDetailsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        showSnackBar(response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                ProgressDialogUtil.hideProgressDialog();
                showToast(t.getMessage());
            }
        });
    }


    @Override
    protected void setContextView() {
        setContentView(R.layout.activity_login_details);
    }


    private EditText getNameEdt() {
        return (EditText) findViewById(R.id.name_edt);
    }

    private EditText getLocationEdt() {
        return (EditText) findViewById(R.id.location_edt);
    }

    private EditText getAddressEdt() {
        return (EditText) findViewById(R.id.address_edt);
    }
}
