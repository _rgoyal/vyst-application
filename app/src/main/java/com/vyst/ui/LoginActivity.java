package com.vyst.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.vyst.MainActivity;
import com.vyst.R;
import com.vyst.model.LoginBody;
import com.vyst.model.LoginData;
import com.vyst.model.LoginModel;
import com.vyst.network.NetworkAdaper;
import com.vyst.utils.AppConstant;
import com.vyst.utils.BaseActivity;
import com.vyst.utils.PrefHelper;
import com.vyst.utils.ProgressDialogUtil;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {


    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;


    private int timeLimit = 30;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;


    private TextInputLayout inputLayoutPhone;
    private TextInputLayout inputLayoutOtp;
    private TextView resendBtn;
    private TextView otpBtn;
    private TextView loginBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Restore instance state
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }
        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        initView();

        setClickListeners();

        hideLayout();

        addTextChangeListners();

        addActionDoneEventForPhone(getPhoneNumber());
        addActionDoneEventForLogin(getEdtOTp());

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                ProgressDialogUtil.showProgressDialog(context);
                getEdtOTp().setText(credential.getSmsCode());
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    showSnackBar("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    showSnackBar("Invalid phone number.");
                }


            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                showSnackBar("OTP sent");
                setCountDownTimer();
                // [START_EXCLUDE]
                // Update UI
                // [END_EXCLUDE]
            }
        };
        // [END phone_auth_callbacks]
    }

    private void setCountDownTimer() {
        try {
            new CountDownTimer(30000, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    resendBtn.setText("0:" + checkDigit(timeLimit));
                    resendBtn.setEnabled(false);
                    timeLimit--;
                }

                @Override
                public void onFinish() {
                    resendBtn.setEnabled(true);
                    resendBtn.setText("Resend Code");
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    private void addActionDoneEventForPhone(EditText edtPhone) {
        edtPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    otpBtn.performClick();
                }
                return false;
            }
        });
    }

    private void addActionDoneEventForLogin(EditText edtOtp) {
        edtOtp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginBtn.performClick();
                }
                return false;
            }
        });
    }


    private void addTextChangeListners() {
        getPhoneNumber().addTextChangedListener(new MyTextWatcher(getPhoneNumber()));
    }

    private void hideLayout() {
        inputLayoutOtp.setVisibility(View.GONE);
        resendBtn.setVisibility(View.GONE);
        loginBtn.setVisibility(View.GONE);
        otpBtn.setVisibility(View.VISIBLE);
    }


    private void visibleLayout() {
        inputLayoutOtp.setVisibility(View.VISIBLE);
        resendBtn.setVisibility(View.VISIBLE);
        loginBtn.setVisibility(View.VISIBLE);
        otpBtn.setVisibility(View.GONE);
    }

    private void setClickListeners() {

        resendBtn.setOnClickListener(this);
        otpBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
    }

    private void initView() {
        inputLayoutPhone = (TextInputLayout) findViewById(R.id.input_layout_phone);
        inputLayoutOtp = (TextInputLayout) findViewById(R.id.input_layout_otp);
        resendBtn = (TextView) findViewById(R.id.resend_btn);
        otpBtn = (TextView) findViewById(R.id.otp_btn);
        loginBtn = (TextView) findViewById(R.id.login_btn);

    }


    private EditText getPhoneNumber() {
        return (EditText) findViewById(R.id.phoneNumber);
    }

    private EditText getEdtOTp() {
        return (EditText) findViewById(R.id.edtOTp);
    }


    @Override
    protected void setContextView() {
        setContentView(R.layout.activity_login);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.resend_btn:
                if (validatePhone()) {
                    resendVerificationCode(getPhoneNumber().getText().toString().trim(), mResendToken);
                }
                break;

            case R.id.otp_btn:
                if (validatePhone()) {
                    visibleLayout();
                    startPhoneNumberVerification(getPhoneNumber().getText().toString().trim());
                }
                break;

            case R.id.login_btn:
                if (validateOTP()) {
                    verifyPhoneNumberWithCode(mVerificationId, getEdtOTp().getText().toString().trim());
                }
                break;
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                timeLimit,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }


    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                timeLimit,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks

    }
    // [END resend_verification]

    private boolean validatePhone() {
        if (getPhoneNumber().getText().toString().trim().isEmpty()) {
            inputLayoutPhone.setError(getString(R.string.str_booknow_fragment_enter_mobile));
            return false;
        } else if (getPhoneNumber().getText().toString().trim().length() != 10) {
            inputLayoutPhone.setError(getString(R.string.str_booknow_fragment_mobile_valiation));
            return false;
        } else {
            inputLayoutPhone.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateOTP() {
        if (getEdtOTp().getText().toString().trim().isEmpty()) {
            inputLayoutOtp.setError(getString(R.string.str_enter_otp));
            return false;
        } else {
            inputLayoutOtp.setErrorEnabled(false);
        }

        return true;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.phoneNumber:
                    validatePhone();

                    hideLayout();
                    break;

            }
        }
    }


    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        ProgressDialogUtil.hideProgressDialog();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            // [START_EXCLUDE]
//                            showSnackBar("Login Successful.");

                            callNetworkForLogin(user.getPhoneNumber());

//                            moveToProfile(user.getPhoneNumber());
                            // [END_EXCLUDE]
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                // [END_EXCLUDE]
                            }
                            showSnackBar("Invalid code. Try again!");
                            // [START_EXCLUDE silent]
                            // Update UI
                            // [END_EXCLUDE]
                        }
                    }
                });
    }

    private void callNetworkForLogin(final String phoneNumber) {
        ProgressDialogUtil.showProgressDialog(context);

        LoginBody loginBody = new LoginBody();
        loginBody.setPhone(phoneNumber);

        NetworkAdaper.getInstance().getNetworkServices().loginUser("application/json", loginBody).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                ProgressDialogUtil.hideProgressDialog();
                try {


                    if (response.body().getStatus() == 200) {

                        if(response.body().getData().getUserId()!=null){

                            saveDetails(response.body().getData());
                            Intent intent= new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else {
                            moveToProfile(phoneNumber);
                        }
                    } else {
                        showSnackBar(response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                ProgressDialogUtil.hideProgressDialog();
                t.getStackTrace();
                showToast(t.getMessage());
            }
        });
    }

    private void saveDetails(LoginData data) {
        if (data != null) {
            try {
                PrefHelper.getInstance().setUserId(data.getUserId());
                PrefHelper.getInstance().storeSharedValue(AppConstant.TOKEN, data.getToken());
                PrefHelper.getInstance().storeSharedValue(AppConstant.PHONE, data.getPhone());
                PrefHelper.getInstance().storeSharedValue(AppConstant.NAME, data.getName());
                PrefHelper.getInstance().storeSharedValue(AppConstant.LOCATION_ID, String.valueOf(data.getLocationId()));
                PrefHelper.getInstance().storeSharedValue(AppConstant.LOCATION_ADDRESS, data.getLocationAddress());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void moveToProfile(String phoneNumber) {
        Intent intent_home = new Intent(LoginActivity.this, LoginDetailsActivity.class);
        intent_home.putExtra(AppConstant.PHONE, phoneNumber);
        startActivity(intent_home);
    }
    // [END sign_in_with_phone]


    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        ProgressDialogUtil.showProgressDialog(context);
        signInWithPhoneAuthCredential(credential);
        // [END verify_with_code]
    }

    @Override
    protected void onStart() {
        super.onStart();
        // [START_EXCLUDE]
        if (mVerificationInProgress && validatePhone()) {
            startPhoneNumberVerification(getPhoneNumber().getText().toString().trim());
        }
        // [END_EXCLUDE]
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
