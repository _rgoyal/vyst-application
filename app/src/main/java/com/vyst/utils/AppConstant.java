package com.vyst.utils;

/**
 * Created by ppard on 12/10/2017.
 */

public class AppConstant {
    public final static String HEADER = "HEADER";
    public final static String USERNAME = "USERNAME";
    public final static String USER_ID = "USER_ID";
    public final static String PHONE = "PHONE";
    public final static String TOKEN = "TOKEN";
    public final static String NAME = "NAME";
    public final static String LOCATION_ID = "LOCATION_ID";
    public final static String LOCATION_ADDRESS = "LOCATION_ADDRESS";
    public final static String GET_LATITUDE = "GET_LATITUDE";
    public final static String GET_LONGITUDE = "GET_LONGITUDE";
    public final static String DEVICE_TOKEN = "DEVICE_TOKEN ";
    public final static String confirm = "confirm";
    public static final int LOCATION_REQUEST_CODE = 10;
}
