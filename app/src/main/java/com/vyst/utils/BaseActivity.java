package com.vyst.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;

/**
 * Created by root on 22/5/17.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private final String INFO = "INFO";
    private final String ERROR = "ERROR";
    private final String DEBUG = "DEBUG";

    protected Context context;
    protected Gson oGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContextView();
        setUpHelper();
        hideKeyBoardAlways();
    }


    /**
     * Hide Keyboard
     *
     * @return
     */

    private void hideKeyBoardAlways() {
        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
    }

    private void setUpHelper() {
        context = this;
        oGson = new Gson();
    }


    protected abstract void setContextView();


    /**
     * Check network connection
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    /**
     * Show toast message
     *
     * @param message
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar
                .make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }



}
