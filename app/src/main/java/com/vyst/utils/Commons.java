package com.vyst.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by ppard on 12/10/2017.
 */

public class Commons {


    public static boolean isValidEmaillId(String email){

        return Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches();
    }

    public static String getApplicationVersion(Context mContext) {

        try {
            PackageInfo packageInfo = mContext.getApplicationContext().getPackageManager().getPackageInfo(
                    mContext.getApplicationContext().getPackageName(), 0);
            return Integer.toString(packageInfo.versionCode);

        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Version", "Package code not found", e);
        };
        return null;
    }

    public static void hideSoftKeyboard(@NonNull Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager)mContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static String parseDate(String date) {
        String inputFormat = "dd-MM-yyyy";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

        try {
            return inputParser.format(inputParser.parse(date));
        } catch (java.text.ParseException e) {
            return "";
        }
    }

    public static String getDateFromIso(String date) {
        String inputFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String outputFormat="yyyy-MM-dd";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);
        SimpleDateFormat outputParser = new SimpleDateFormat(outputFormat);

        try {
            return outputParser.format(inputParser.parse(date));
        } catch (java.text.ParseException e) {
            return "";
        }
    }

    public static double getDistance(LatLng customerLatLng, LatLng destinationLatLng) {

        return round(distance(customerLatLng.latitude,customerLatLng.longitude,destinationLatLng.latitude,destinationLatLng.longitude,"K"),2);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private  static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {

        double theta = lon1 - lon2;

        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);

        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts decimal degrees to radians						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::	This function converts radians to decimal degrees						 :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private  static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }


    public static String setFirstCapWord(String word){

        if(word!=null && !word.isEmpty()){
            String upperString = word.substring(0,1).toUpperCase() + word.substring(1);
            return upperString;
        }else {
            return "";
        }
    }

    public static String capitaliseFirstLetterOfEachWord(String value)
    {
        if(value!=null && !value.isEmpty()){
            String[] wordSplit=value.split(" ");
            StringBuilder sb=new StringBuilder();

            for (int i=0;i<wordSplit.length;i++){

                sb.append(wordSplit[i].substring(0,1).toUpperCase().
                        concat(wordSplit[i].substring(1)).concat(" "));
            }
            return String.valueOf(sb);
        }
        return "";
    }


    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    public static void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
