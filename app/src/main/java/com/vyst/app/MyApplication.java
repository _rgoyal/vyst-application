package com.vyst.app;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.vyst.network.NetworkAdaper;
import com.vyst.utils.PrefHelper;


/**
 * Created by ppard on 12/2/2017.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initSingletons();
        initPicasso();
    }

    private void initSingletons() {
        NetworkAdaper.initInstance();
        PrefHelper.initInstance(this);
        // Obtain the FirebaseAnalytics instance.
        FirebaseAnalytics.getInstance(this);

    }


    private void initPicasso() {

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }
}
