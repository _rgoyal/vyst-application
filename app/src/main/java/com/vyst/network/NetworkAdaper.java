package com.vyst.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ppard on 12/2/2017.
 */

public class NetworkAdaper {

    public static NetworkAdaper cInstance;
    public ApiService apiService;

    /* Static 'instance' method */
    public static NetworkAdaper getInstance() {
        return cInstance;
    }

    public static void initInstance() {
        if (cInstance == null) {
            cInstance = new NetworkAdaper();
            cInstance.setupRetrofitClient();
        }
    }

    public void setupRetrofitClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetworkConstant.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public ApiService getNetworkServices() {
        return apiService;
    }
}
