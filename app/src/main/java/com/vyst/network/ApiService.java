package com.vyst.network;

import com.vyst.model.BodyForTokenUserId;
import com.vyst.model.CartListModel;
import com.vyst.model.LocationModel;
import com.vyst.model.LoginBody;
import com.vyst.model.LoginModel;
import com.vyst.model.ProductResponseModel;
import com.vyst.model.RegisterModel;
import com.vyst.model.RegisterPostData;

import org.json.JSONObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by ppard on 12/2/2017.
 */

public interface ApiService {

    @POST("location/list")
    Call<LocationModel> getLocationList();

    @POST("user/login")
    Call<LoginModel> loginUser(@Header("Content-Type") String contentType, @Body LoginBody loginBody);

    @POST("user/register")
    Call<RegisterModel> register(@Header("Content-Type") String contentType, @Body RegisterPostData registerPostData);

    @POST("item/list")
    Call<ProductResponseModel> getProductList(@Header("Content-Type") String contentType, @Body BodyForTokenUserId bodyForTokenUserId);

    @POST("cart/list")
    Call<CartListModel> getCartList(@Header("Content-Type") String contentType, @Body BodyForTokenUserId bodyForTokenUserId);

}
