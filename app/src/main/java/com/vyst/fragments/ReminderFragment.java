package com.vyst.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.vyst.R;
import com.vyst.reminder.ReminderEditActivity;
import com.vyst.reminder.RemindersDbAdapter;

/**
 * Created by ppard on 2/20/2018.
 */

public class ReminderFragment extends Fragment implements View.OnClickListener {


    View rootView;
    MyCursorAdapter customAdapter;

    private static final int ACTIVITY_CREATE = 0;
    private static final int ACTIVITY_EDIT = 1;

    private RemindersDbAdapter mDbHelper;

    private ListView listView;
    private TextView noDataFound;
    private Button addReminderBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHelper = new RemindersDbAdapter(getActivity());
        mDbHelper.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_reminder, container, false);

        listView = (ListView) rootView.findViewById(R.id.reminder_listview);
        addReminderBtn = (Button) rootView.findViewById(R.id.add_reminder_btn);
        addReminderBtn.setOnClickListener(this);

        fillData();


        return rootView;
    }


    private void fillData() {
        final Cursor remindersCursor = mDbHelper.fetchAllReminders();

        if (remindersCursor.getCount() > 1) {
            addReminderBtn.setVisibility(View.GONE);
        } else {
            addReminderBtn.setVisibility(View.VISIBLE);
        }

        getActivity().startManagingCursor(remindersCursor);

        // Create an array to specify the fields we want to display in the list (only TITLE)
        String[] from = new String[]{RemindersDbAdapter.KEY_TITLE};

        // and an array of the fields we want to bind those fields to (in this case just text1)

        // Now create a simple cursor adapter and set it to display
        new Handler().post(new Runnable() {

            @Override
            public void run() {
                customAdapter = new MyCursorAdapter(getActivity(), remindersCursor);

                listView.setAdapter(customAdapter);
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_reminder_btn:
                createReminder();
                break;
        }
    }

    private void createReminder() {
        Intent i = new Intent(getActivity(), ReminderEditActivity.class);
        startActivityForResult(i, ACTIVITY_CREATE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        fillData();
    }

    public class MyCursorAdapter extends CursorAdapter {
        public MyCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        // The newView method is used to inflate a new view and return it,
        // you don't bind any data to the view at this point.
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.reminder_row, parent, false);
        }

        // The bindView method is used to bind all data to a given view
        // such as setting the text on a TextView.
        @Override
        public void bindView(View view, Context context, final Cursor cursor) {
            // Find fields to populate in inflated template
            TextView title = (TextView) view.findViewById(R.id.title_txt);
            TextView message = (TextView) view.findViewById(R.id.message);
            TextView date = (TextView) view.findViewById(R.id.date);
            TextView remove_btn = (TextView) view.findViewById(R.id.remove_btn);
            String strTitle = null;
            try {
                strTitle = cursor.getString(cursor.getColumnIndexOrThrow(RemindersDbAdapter.KEY_TITLE));
                title.setText(strTitle);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            String strmsg = null;
            try {
                strmsg = cursor.getString(cursor.getColumnIndexOrThrow(RemindersDbAdapter.KEY_BODY));
                message.setText(strmsg);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            String strDate = null;
            try {
                strDate = cursor.getString(cursor.getColumnIndexOrThrow(RemindersDbAdapter.KEY_DATE_TIME));
                date.setText(strDate);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            // Populate fields with extracted properties

            final int position = cursor.getPosition();

            remove_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    cursor.moveToPosition(position);
                    mDbHelper.deleteReminder(Long.parseLong(cursor.getString(cursor.getColumnIndexOrThrow(RemindersDbAdapter.KEY_ROWID))));
                    fillData();
                }
            });
        }

    }

}
