package com.vyst.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vyst.R;
import com.vyst.model.BodyForTokenUserId;
import com.vyst.model.ProductData;
import com.vyst.model.ProductResponseModel;
import com.vyst.network.NetworkAdaper;
import com.vyst.network.NetworkConstant;
import com.vyst.utils.Commons;
import com.vyst.utils.ProgressDialogUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ppard on 2/20/2018.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    View rootView;
    private ListView productList;
    private List<ProductData> products;
    Adapter adapter;
    private EditText searchEdit;
    private ImageButton cross_btn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        products = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        Commons.hideSoftKeyboard(getActivity());

        initView();

        callNetworkForProducts();

        setAdapter(products);

        return rootView;
    }

    private void callNetworkForProducts() {

        ProgressDialogUtil.showProgressDialog(getActivity());

        BodyForTokenUserId bodyForTokenUserId = new BodyForTokenUserId();
        bodyForTokenUserId.setToken("4e465c13-8c9c-4065-a4e8-f8ce2a76d738");
        bodyForTokenUserId.setUser_id("UDC41CV2L3DIR54");

        NetworkAdaper.getInstance().getNetworkServices().getProductList("application/json", bodyForTokenUserId).enqueue(new Callback<ProductResponseModel>() {
            @Override
            public void onResponse(Call<ProductResponseModel> call, Response<ProductResponseModel> response) {
                ProgressDialogUtil.hideProgressDialog();
                try {
                    products.clear();
                    if (response.body().getStatus() == 200) {
                        if (response.body().getData() != null && response.body().getData().size() != 0) {
                            products = response.body().getData();
                            setAdapter(response.body().getData());
                        } else {
                            Commons.showSnackBar(rootView, response.body().getMessage());
                        }
                    } else {
                        Commons.showSnackBar(rootView, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ProductResponseModel> call, Throwable t) {
                ProgressDialogUtil.hideProgressDialog();
                Commons.showToast(getActivity(), t.getMessage());
            }
        });
    }

    private void initView() {
        productList = (ListView) rootView.findViewById(R.id.product_list);
        searchEdit = (EditText) rootView.findViewById(R.id.searchEdit);
        cross_btn = (ImageButton) rootView.findViewById(R.id.cross_btn);
        cross_btn.setOnClickListener(this);
        addSearchTextChangeProduct();
    }

    public void addSearchTextChangeProduct() {
        searchEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                List<ProductData> filterList = new ArrayList<ProductData>();
                for (int i = 0; i < products.size(); i++) {

                    String name = products.get(i).getItemName().toString().toLowerCase();
                    if (name.contains(s.toString().toLowerCase())) {
                        filterList.add(products.get(i));
                    }

                }

                if (s.length() == 0) {
                    adapter.update(products);
                } else {
                    if (filterList != null && filterList.size() != 0) {
                        adapter.update(filterList);
                    } else {
                        ProductData model1 = new ProductData();
                        model1.setItemName(getResources().getString(R.string.str_no_data_found));
                        filterList.add(model1);
                        adapter.update(filterList);
                    }
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


            }
        });
    }

    private void setAdapter(List<ProductData> products) {
        if (products != null && products.size() != 0) {
            adapter = new Adapter(getActivity(), products);
            productList.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cross_btn:
                searchEdit.setText("");
                break;
        }
    }


    class Adapter extends BaseAdapter {
        public List<ProductData> products;
        Activity context;
        LayoutInflater l;
        ViewHolder holder;

        public Adapter(Activity context, List<ProductData> products) {
            this.products = products;
            this.context = context;
            l = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(List<ProductData> products) {
            this.products = products;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = l.inflate(R.layout.adapter_product_child_layout, null);
                holder = new ViewHolder();
                holder.productName = (TextView) convertView.findViewById(R.id.product_title);
                holder.parentLayout = (RelativeLayout) convertView.findViewById(R.id.parent_layout);
                holder.productImage = (ImageView) convertView.findViewById(R.id.product_image);
                holder.descTxt = (TextView) convertView.findViewById(R.id.desc_txt);
                holder.relOfferPrice = (RelativeLayout) convertView.findViewById(R.id.rel_offer_price);
                holder.rupee = (TextView) convertView.findViewById(R.id.rupee);
                holder.itemsPrice = (TextView) convertView.findViewById(R.id.items_price);
                holder.relMrpOfferPrice = (RelativeLayout) convertView.findViewById(R.id.rel_mrp_offer_price);
                holder.rupee2 = (TextView) convertView.findViewById(R.id.rupee2);
                holder.itemsMrpPrice = (TextView) convertView.findViewById(R.id.items_mrp_price);
                holder.quantityLayout = (LinearLayout) convertView.findViewById(R.id.quantity_layout);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (position % 2 == 0) {
                holder.parentLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            } else {
                holder.parentLayout.setBackgroundColor(Color.parseColor("#dfdfdf"));
            }

            final ProductData model = products.get(position);
            try {
                String data = (model.getItemName() != null && !model.getItemName().isEmpty() ? model.getItemName() : "");
                holder.productName.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String data = (model.getDescription() != null && !model.getDescription().isEmpty() ? model.getDescription() : "");
                holder.descTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String data = String.valueOf((model.getPrice() != null && model.getPrice() != 0 ? model.getPrice() : ""));
                holder.rupee.setText(getResources().getString(R.string.str_rupee_symbol));
                holder.itemsPrice.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Picasso.with(getActivity()).load(NetworkConstant.IMAGE_BASE + model.getFileName()).resize(70, 70)
                        .centerCrop()
                        //                    .placeholder(Your Drawable Resource) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.no_image)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.productImage);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }


        class ViewHolder {
            public TextView productName;
            private RelativeLayout parentLayout;
            private ImageView productImage;
            private TextView descTxt;
            private RelativeLayout relOfferPrice;
            private TextView rupee;
            private TextView itemsPrice;
            private RelativeLayout relMrpOfferPrice;
            private TextView rupee2;
            private TextView itemsMrpPrice;
            private LinearLayout quantityLayout;
        }
    }

}
