package com.vyst.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vyst.R;
import com.vyst.model.BodyForTokenUserId;
import com.vyst.model.CartList;
import com.vyst.model.CartListModel;
import com.vyst.model.CartProduct;
import com.vyst.network.NetworkAdaper;
import com.vyst.network.NetworkConstant;
import com.vyst.utils.Commons;
import com.vyst.utils.ProgressDialogUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ppard on 2/20/2018.
 */

public class CartFragment extends Fragment implements View.OnClickListener {


    View rootView;
    private ListView cart_list;
    private List<CartList> products;
    Adapter adapter;
    private EditText searchEdit;
    private TextView totalTxt;
    private TextView discountTxt;
    private TextView deliveryTxt;
    private TextView actualTotalTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        products = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_cart, container, false);

        Commons.hideSoftKeyboard(getActivity());

        initView();

        callNetworkForCart();

        setAdapter(products);

        return rootView;
    }

    private void callNetworkForCart() {

        ProgressDialogUtil.showProgressDialog(getActivity());

        BodyForTokenUserId bodyForTokenUserId = new BodyForTokenUserId();
        bodyForTokenUserId.setToken("4e465c13-8c9c-4065-a4e8-f8ce2a76d738");
        bodyForTokenUserId.setUser_id("UDC41CV2L3DIR54");

        NetworkAdaper.getInstance().getNetworkServices().getCartList("application/json", bodyForTokenUserId).enqueue(new Callback<CartListModel>() {
            @Override
            public void onResponse(Call<CartListModel> call, Response<CartListModel> response) {
                ProgressDialogUtil.hideProgressDialog();
                try {
                    products.clear();
                    if (response.body().getStatus() == 200) {
                        if (response.body().getCartProduct() != null && response.body().getCartProduct().size() != 0) {
                            CartProduct cartProduct = response.body().getCartProduct().get(0);
                            products = cartProduct.getProductList();

                            setViewValues(cartProduct);
                            setAdapter(cartProduct.getProductList());
                        } else {
                            Commons.showSnackBar(rootView, response.body().getMessage());
                        }
                    } else {
                        Commons.showSnackBar(rootView, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CartListModel> call, Throwable t) {
                ProgressDialogUtil.hideProgressDialog();
                Commons.showToast(getActivity(), t.getMessage());
            }
        });
    }


    private void initView() {
        cart_list = (ListView) rootView.findViewById(R.id.cart_list);
        searchEdit = (EditText) rootView.findViewById(R.id.searchEdit);

        rootView.findViewById(R.id.applyCoupen_1).setOnClickListener(this);
        totalTxt = (TextView) rootView.findViewById(R.id.total_txt);
        discountTxt = (TextView) rootView.findViewById(R.id.discount_txt);
        deliveryTxt = (TextView) rootView.findViewById(R.id.delivery_txt);
        actualTotalTxt = (TextView) rootView.findViewById(R.id.actual_total_txt);
        rootView.findViewById(R.id.add_reminder_btn).setOnClickListener(this);
    }


    private void setAdapter(List<CartList> products) {
        if (products != null && products.size() != 0) {
            adapter = new Adapter(getActivity(), products);
            cart_list.setAdapter(adapter);
        }
    }

    private void setViewValues(CartProduct cartProduct) {
        if (cartProduct != null ) {

            try {
                String data = (cartProduct.getBill() != null && !cartProduct.getBill().isEmpty() ? getRuppeSymbol()+cartProduct.getBill() : "");
                totalTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                String data = (cartProduct.getDiscount() != null && !cartProduct.getDiscount().isEmpty() ? getRuppeSymbol()+cartProduct.getDiscount() : "");
                discountTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String data = (cartProduct.getTotalPayableAmount() != null && !cartProduct.getTotalPayableAmount().isEmpty() ? getRuppeSymbol()+cartProduct.getTotalPayableAmount() : "");
                actualTotalTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private String getRuppeSymbol(){
        return getResources().getString(R.string.str_rupee_symbol);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.applyCoupen_1:
                //TODO implement
                break;
            case R.id.add_reminder_btn:
                //TODO implement
                break;
        }
    }


    class Adapter extends BaseAdapter {
        public List<CartList> products;
        Activity context;
        LayoutInflater l;
        ViewHolder holder;

        public Adapter(Activity context, List<CartList> products) {
            this.products = products;
            this.context = context;
            l = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(List<CartList> products) {
            this.products = products;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return products.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = l.inflate(R.layout.adapter_cart_list_child_layout, null);
                holder = new ViewHolder();
                holder.productName = (TextView) convertView.findViewById(R.id.product_title);
                holder.parentLayout = (RelativeLayout) convertView.findViewById(R.id.parent_layout);
                holder.productImage = (ImageView) convertView.findViewById(R.id.product_image);
                holder.descTxt = (TextView) convertView.findViewById(R.id.desc_txt);
                holder.relOfferPrice = (RelativeLayout) convertView.findViewById(R.id.rel_offer_price);
                holder.rupee = (TextView) convertView.findViewById(R.id.rupee);
                holder.itemsPrice = (TextView) convertView.findViewById(R.id.items_price);
                holder.relMrpOfferPrice = (RelativeLayout) convertView.findViewById(R.id.rel_mrp_offer_price);
                holder.rupee2 = (TextView) convertView.findViewById(R.id.rupee2);
                holder.itemsMrpPrice = (TextView) convertView.findViewById(R.id.items_mrp_price);
                holder.qty_value = (TextView) convertView.findViewById(R.id.qty_value);
                holder.quantityLayout = (LinearLayout) convertView.findViewById(R.id.quantity_layout);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            final CartList model = products.get(position);
            try {
                String data = (model.getItemName() != null && !model.getItemName().isEmpty() ? model.getItemName() : "");
                holder.productName.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String data = (model.getDescription() != null && !model.getDescription().isEmpty() ? model.getDescription() : "");
                holder.descTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String data = (model.getQnt() != null && !model.getQnt().isEmpty() ? model.getQnt() : "");
                holder.qty_value.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                String data = String.valueOf((model.getPrice() != null && model.getPrice() != 0 ? model.getPrice() : ""));
                holder.rupee.setText(getResources().getString(R.string.str_rupee_symbol));
                holder.itemsPrice.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Picasso.with(getActivity()).load(NetworkConstant.IMAGE_BASE + model.getFileName()).resize(70, 70)
                        .centerCrop()
                        //                    .placeholder(Your Drawable Resource) //this is optional the image to display while the url image is downloading
                        .error(R.drawable.no_image)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                        .into(holder.productImage);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return convertView;
        }


        class ViewHolder {
            public TextView productName;
            private RelativeLayout parentLayout;
            private ImageView productImage;
            private TextView descTxt;
            private RelativeLayout relOfferPrice;
            private TextView rupee;
            private TextView itemsPrice;
            private RelativeLayout relMrpOfferPrice;
            private TextView rupee2;
            private TextView itemsMrpPrice, qty_value;
            private LinearLayout quantityLayout;
        }
    }

}
