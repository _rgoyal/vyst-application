package com.vyst.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.vyst.R;
import com.vyst.model.OrderHistoryData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ppard on 2/20/2018.
 */

public class OrderFragment extends Fragment {


    View rootView;
    private ListView ordersListView;
    Adapter adapter;
    private List<OrderHistoryData> orders;

    String[] strOrder = {"Order 1", "Order 2", "Order 3", "Order 4", "Order 5", "Order 6"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orders = new ArrayList<>();

        for (int i = 0; i < strOrder.length; i++) {
            OrderHistoryData data = new OrderHistoryData();
            data.setTitle(strOrder[i]);
            orders.add(data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_order, container, false);

        initView();

        setAdapter(orders);

        return rootView;
    }

    private void initView() {
        ordersListView = (ListView) rootView.findViewById(R.id.orders_listview);

    }

    private void setAdapter(List<OrderHistoryData> orders) {

        if (orders != null && !orders.isEmpty()) {
            adapter = new Adapter(getActivity(), orders);
            ordersListView.setAdapter(adapter);
        }

    }


    class Adapter extends BaseAdapter {
        public List<OrderHistoryData> invitee;
        Activity context;
        LayoutInflater l;
        ViewHolder holder;

        public Adapter(Activity context, List<OrderHistoryData> invitee) {
            this.invitee = invitee;
            this.context = context;
            l = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(List<OrderHistoryData> invitee) {
            this.invitee = invitee;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return invitee.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = l.inflate(R.layout.adapter_order_history_child_layout, null);
                holder = new ViewHolder();
                holder.titleTxt = (TextView) convertView.findViewById(R.id.title_txt);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            final OrderHistoryData model = invitee.get(position);

            try {
                String data = (model.getTitle() != null && !model.getTitle().isEmpty() ? model.getTitle() : "");
                holder.titleTxt.setText(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }


        class ViewHolder {
            public TextView titleTxt;

        }
    }
}
