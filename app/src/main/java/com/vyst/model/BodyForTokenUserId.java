package com.vyst.model;

/**
 * Created by ppard on 3/7/2018.
 */

public class BodyForTokenUserId {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    private String user_id;
}
