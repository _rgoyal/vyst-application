package com.vyst.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ppard on 2/25/2018.
 */

public class LocationList implements Serializable {

    @SerializedName("location_address")
    @Expose
    private String locationAddress;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
}
