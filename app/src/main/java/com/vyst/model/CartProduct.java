package com.vyst.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ppard on 3/10/2018.
 */

public class CartProduct {

    @SerializedName("bill")
    @Expose
    private String bill;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("product_list")
    @Expose
    private List<CartList> productList = null;
    @SerializedName("total_payable_amount")
    @Expose
    private String totalPayableAmount;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public List<CartList> getProductList() {
        return productList;
    }

    public void setProductList(List<CartList> productList) {
        this.productList = productList;
    }

    public String getTotalPayableAmount() {
        return totalPayableAmount;
    }

    public void setTotalPayableAmount(String totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


}
